import http from '../common/vmeitime-http/interface'
export const api_index = () => {
	return http.request({
		url: 'mobile/index',
		method: "POST",
		dataType: 'text',
	})
}


export const api_group_list = () => {
	return http.request({
		url: 'mobile/getGameGroup',
		method: 'GET',
	});
}

export const api_game_list = (group_id, page, rows) => {
	return http.request({
		url: 'mobile/getGameList',
		method: 'GET',
		data: {
			group_id: group_id,
			page: page,
			rows: rows
		},
	});
}

export const api_game_paly = (user_id, game_id) => {
	return http.request({
		url: 'mobile/gamePaly',
		method: 'POST',
		data: {
			member_id: user_id,
			game_id: game_id
		}
	});
}

//游戏礼包列表
export const api_gift_list = (group_id, page, rows) => {
	return http.request({
		url: 'mobile/getGiftList',
		method: 'GET',
		data: {
			group_id: group_id,
			page: page,
			rows: rows
		},
	});
}

//领取游戏礼包
export const api_get_gift = (gift_id, member_id) => {
	return http.request({
		url: 'mobile/getGift',
		method: 'POST',
		data: {
			gift_id: gift_id,
			member_id: member_id,
		},
	});
}

//游戏下载

export const api_down_game = (game_id, member_id) => {
	return http.request({
		url: 'mobile/downGame',
		method: 'GET',
		data: {
			game_id: game_id,
			member_id: member_id
		}
	})
}
//我的游戏记录
export const api_mygame = (member_id, page, rows) => {
	return http.request({
		url: 'mobile/myGameList',
		method: 'GET',
		data: {
			member_id: member_id,
			page: page,
			rows: rows
		}
	})
}
